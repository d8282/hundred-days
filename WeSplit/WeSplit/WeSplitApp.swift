//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Hargitai Gabor on 2022. 08. 13..
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
